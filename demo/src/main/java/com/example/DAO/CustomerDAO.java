package com.example.DAO;

import org.springframework.data.repository.CrudRepository;

import com.example.entity.Customer;

public interface CustomerDAO extends CrudRepository<Customer, String>{

}
