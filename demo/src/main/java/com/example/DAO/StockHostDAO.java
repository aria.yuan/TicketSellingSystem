package com.example.DAO;

import org.springframework.data.repository.CrudRepository;

import com.example.entity.StockHost;

public interface StockHostDAO extends CrudRepository<StockHost, Long>{

}
