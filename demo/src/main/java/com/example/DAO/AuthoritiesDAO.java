package com.example.DAO;

import org.springframework.data.repository.CrudRepository;

import com.example.entity.Authorities;

public interface AuthoritiesDAO extends CrudRepository<Authorities, String>{

}
