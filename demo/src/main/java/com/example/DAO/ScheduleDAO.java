package com.example.DAO;

import org.springframework.data.repository.CrudRepository;

import com.example.entity.Schedule;

public interface ScheduleDAO extends CrudRepository<Schedule, Long>{
	//CrudRepository提供了很多方法，如: save、findAll、findOne、delete，可以不需要使用SQL就可以存取資料庫裡的資料


}