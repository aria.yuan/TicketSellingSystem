package com.example.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@SessionScope
@Component
public class ShoppingCart implements Serializable {
	/**
  * 
  */
	private static final long serialVersionUID = -5494311567944263493L;
	private List<Ticket> cart = new ArrayList<Ticket>();

	public Iterable<Ticket> getCart() {
		return cart;
	}
	
	public int getSize(){
		return cart.size() ;
	}
	
	public Ticket isSold(int i) {
		cart.get(i).setIsSold(1);
		return cart.get(i) ;
	}

	public void add(Ticket ticket) {
		cart.add(ticket);
	}
	
	public void delete(Long id){
		for(int i = 0 ; i < cart.size() ; i++){
			if(cart.get(i).getId().equals(id)){
				cart.remove(i) ;
			}
		}
	}

	public void cleanup() {
		cart = new ArrayList<Ticket>();
	}

}