package com.example.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.example.entity.Ticket;

@Entity
@Table(name = "users")
public class Customer implements Serializable{
	@Id
	private String username;
	private String name ;
	private String password ;
	private String phone ;
	private int enabled ;
	
	@OneToMany(mappedBy = "cId")
	private List<Ticket> tickets;
	
	@OneToOne
	@JoinColumn(name = "username")
	private Customer fk_authorities_users;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public int getEnabled() {
		return enabled;
	}
	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}
	public Iterable<Ticket> getTickets() {
		  return tickets;
		 }
	

}
