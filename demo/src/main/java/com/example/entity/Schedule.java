package com.example.entity;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.example.entity.Ticket;

@Entity
@Table(name = "schedule")
public class Schedule implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private Date time ;
	private static final long serialVersionUID = 3316076651716569539L;
	
	@ManyToOne
	@JoinColumn(name = "s_id")
	private Stock sId;
	
	@OneToMany(mappedBy = "sId")
	private List<Ticket> tickets;
	
	private transient int row;
	private transient int col;

	public Ticket getById(int i){
		return tickets.get(i) ;
	}
	
	public int getTicketSize(){
		return tickets.size() ;
	}
	
	public Ticket getTicketByid(int i){
		return tickets.get(i) ;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Stock getsId() {
		return sId;
	}

	public void setsId(Stock sId) {
		this.sId = sId;
	}
	
	public Iterable<Ticket> getTickets() {
		return tickets;
	}
	
	

}
