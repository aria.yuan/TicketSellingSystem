package com.example.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ticket")
public class Ticket implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private int seat_num;
	private int isSold;
	private String price;
	private static final long serialVersionUID = 3316076651716569539L;
	
	@ManyToOne
	@JoinColumn(name = "c_id")
	private Customer cId;
	
	@ManyToOne
	@JoinColumn(name = "s_id")
	private Schedule sId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getSeat_num() {
		return seat_num;
	}

	public void setSeat_num(int i) {
		this.seat_num = i;
	}

	public int getIsSold() {
		return isSold;
	}

	public void setIsSold(int isSold) {
		this.isSold = isSold;
	}

	public Customer getcId() {
		return cId;
	}

	public void setcId(Customer cId) {
		this.cId = cId;
	}

	public Schedule getsId() {
		return sId;
	}

	public void setsId(Schedule sId) {
		this.sId = sId;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	
}
