package com.example.entity;

import java.util.List;
import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.web.multipart.MultipartFile;

@Entity
// 告訴JPA這是個Entity類別
@Table(name = "stock")
// gaosujap tablemingcheng
public class Stock implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String place;
	private String performers;
	private String name;
	private String price;
	private String discountedPrice;
	private Date discountedDate;
	private static final long serialVersionUID = -2957645392914180170L;

	private transient MultipartFile photoFile;

	private String photo;

	@ManyToOne
	@JoinColumn(name = "category")
	private StockCategory stockCategory;
	
	@ManyToOne
	@JoinColumn(name = "host")
	private StockHost stockHost;

	
	@OneToMany(mappedBy = "sId")
	private List<Schedule> schedules;

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getDiscountedPrice() {
		return discountedPrice;
	}

	public void setDiscountedPrice(String discountedPrice) {
		this.discountedPrice = discountedPrice;
	}

	public Date getDiscountedDate() {
		return discountedDate;
	}

	public void setDiscountedDate(Date discountedDate) {
		this.discountedDate = discountedDate;
	}

	public StockCategory getStockCategory() {
		return stockCategory;
	}

	public void setStockCategory(StockCategory stockCategory) {
		this.stockCategory = stockCategory;
	}

	public StockHost getStockHost() {
		return stockHost;
	}

	public void setStockHost(StockHost stockHost) {
		this.stockHost = stockHost;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPerformers() {
		return performers;
	}

	public void setPerformers(String performers) {
		this.performers = performers;
	}

	public MultipartFile getPhotoFile() {
		return photoFile;
	}

	public void setPhotoFile(MultipartFile photoFile) {
		this.photoFile = photoFile;
	}

	public void setPhoto() {
		this.photo = photoFile.getOriginalFilename();
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Iterable<Schedule> getSchedules() {
		return schedules;
	}

}