package com.example.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.example.entity.Ticket;

@Entity
@Table(name = "authorities")
public class Authorities implements Serializable{
	@Id
	private String username;
	private String authority ;
	
	@OneToOne(mappedBy = "fk_authorities_users")
	private Customer customer;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}
	public Customer getCustomer() {
		return customer;
	}
	

}
