package com.example.controller;

import java.security.Principal;
import java.util.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.example.DAO.CustomerDAO;
import com.example.DAO.ScheduleDAO;
import com.example.DAO.StockCategoryDAO;
import com.example.DAO.StockDAO;
import com.example.DAO.StockHostDAO;
import com.example.DAO.TicketDAO;
import com.example.entity.Customer;
import com.example.entity.Schedule;
import com.example.entity.ShoppingCart;
import com.example.entity.Stock;
import com.example.entity.StockCategory;
import com.example.entity.StockHost;
import com.example.entity.Ticket;
import com.example.storage.StorageService;

@Controller
public class UserController {
	@Autowired
	// 利用@Autowired串連前面的CustomerDAO
	StockDAO dao;

	@Autowired
	StockCategoryDAO categoryDao;

	@Autowired
	TicketDAO ticketDao;

	@Autowired
	CustomerDAO customerDao;

	@Autowired
	StockHostDAO stockHostDao;

	@Autowired
	ScheduleDAO scheduleDao;

	@Autowired
	ShoppingCart cart;

	private final StorageService storageService;

	@Autowired
	public UserController(StorageService storageService) {
		this.storageService = storageService;
	}

	@GetMapping("/user/files/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

		Resource file = storageService.loadAsResource(filename);
		return ResponseEntity
				.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION,
						"attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}

	@RequestMapping(value = { "/user/stockRetrieveAll" }, method = RequestMethod.GET)
	public ModelAndView retrieveCustomers() throws SQLException {
		ModelAndView model = new ModelAndView("user/userList");
		Iterable<StockCategory> categories = categoryDao.findAll();
		model.addObject("allStockCategories", categories);
		StockCategory category = categories.iterator().next();
		model.addObject("stockCategory", category);

		Iterable<StockHost> hosts = stockHostDao.findAll();
		model.addObject("allStockHosts", hosts);
		StockHost host = hosts.iterator().next();
		model.addObject("stockHost", host);

		Iterable<Stock> stocks = dao.findAll();
		model.addObject("stocks", stocks);
		return model;
	}

	@RequestMapping(value = { "/user/stockRetrieveByCategory" }, method = RequestMethod.POST)
	public ModelAndView retrieveStocksByCategory(
			@RequestParam(value = "id", required = false, defaultValue = "1") Long id) {

		ModelAndView model = new ModelAndView("user/userList");

		Iterable<StockHost> hosts = stockHostDao.findAll();
		model.addObject("allStockHosts", hosts);
		StockHost host = hosts.iterator().next();
		model.addObject("stockHost", host);

		Iterable<StockCategory> categories = categoryDao.findAll();
		model.addObject("allStockCategories", categories);
		StockCategory category = categoryDao.findOne(id);
		model.addObject("stockCategory", category);

		model.addObject("stocks", category.getStocks());
		return model;
	}

	@RequestMapping(value = { "/user/stockRetrieveByHost" }, method = RequestMethod.POST)
	public ModelAndView retrieveStocksByHost(
			@RequestParam(value = "id", required = false, defaultValue = "1") Long id) {

		ModelAndView model = new ModelAndView("user/userList");
		Iterable<StockCategory> categories = categoryDao.findAll();
		model.addObject("allStockCategories", categories);
		StockCategory category = categories.iterator().next();
		model.addObject("stockCategory", category);

		Iterable<StockHost> hosts = stockHostDao.findAll();
		model.addObject("allStockHosts", hosts);
		StockHost host = stockHostDao.findOne(id);
		model.addObject("stockHost", host);

		model.addObject("stocks", host.getStocks());
		return model;
	}

	@RequestMapping(value = "/user/scheduleDetail", method = RequestMethod.GET)
	public ModelAndView scheduleDetail(
			@RequestParam(value = "id", required = false) Long id) {
		ModelAndView model = new ModelAndView("user/scheduleDetail");
		Stock stock = dao.findOne(id);
		model.addObject("stock", stock);
		Iterable<Schedule> schedules = stock.getSchedules();
		model.addObject("schedules", schedules);
		return model;
	}

	@RequestMapping(value = { "/user/stockPickSeat" }, method = RequestMethod.GET)
	public ModelAndView stockPickSeat(
			@RequestParam(value = "id", required = false, defaultValue = "1") Long id) {

		ModelAndView model = new ModelAndView("user/pickSeat");
		Schedule schedule = scheduleDao.findOne(id);
		model.addObject("schedule", schedule);
		ArrayList<Ticket> unSold = new ArrayList<Ticket>();

		for (int i = 0; i < schedule.getTicketSize(); i++) {
			if (schedule.getById(i).getIsSold() == 0) {
				unSold.add(schedule.getById(i));
			}
		}
		model.addObject("tickets", unSold);

		Date now = new Date();
		if (now.before(schedule.getsId().getDiscountedDate())) {
			model.addObject("price", schedule.getsId().getDiscountedPrice());
		} else {
			model.addObject("price", schedule.getsId().getPrice());
		}

		System.out.println(now);
		return model;
	}

	@RequestMapping(value = "/user/shoppingCartAdd", method = RequestMethod.GET)
	public ModelAndView addShoppingCart(
			@RequestParam(value = "id", required = false, defaultValue = "1") Long id,
			@RequestParam(value = "price", required = false, defaultValue = "1") String price) {
		Ticket ticket = ticketDao.findOne(id);
		ticket.setIsSold(1);
		ticket.setPrice(price);
		ticketDao.save(ticket);
		ModelAndView model = new ModelAndView("user/pickSeat");
		cart.add(ticket);

		Schedule schedule = ticketDao.findOne(id).getsId();
		model.addObject("schedule", schedule);
		ArrayList<Ticket> unSold = new ArrayList<Ticket>();

		for (int i = 0; i < schedule.getTicketSize(); i++) {
			if (schedule.getById(i).getIsSold() == 0) {
				unSold.add(schedule.getById(i));
			}
		}
		model.addObject("tickets", unSold);

		Date now = new Date();
		if (now.before(schedule.getsId().getDiscountedDate())) {
			model.addObject("price", schedule.getsId().getDiscountedPrice());
		} else {
			model.addObject("price", schedule.getsId().getPrice());
		}

		System.out.println(now);
		return model;
	}

	@RequestMapping(value = "/user/shoppingCartDelete", method = RequestMethod.GET)
	public ModelAndView deleteCustomer(
			@RequestParam(value = "id", required = false, defaultValue = "1") Long id) {
		Ticket ticket = ticketDao.findOne(id);
		ticket.setIsSold(0);
		ticket.setPrice("");
		ticketDao.save(ticket);
		ModelAndView model = new ModelAndView("redirect:/user/shoppingCartList");
		cart.delete(id);
		return model;
	}

	@RequestMapping(value = "/user/shoppingCartList", method = RequestMethod.GET)
	public ModelAndView showShoppingCart() {
		ModelAndView model = new ModelAndView("/user/shoppingCart");
		Iterable<StockCategory> categories = categoryDao.findAll();
		model.addObject("allStockCategories", categories);
		StockCategory category = categories.iterator().next();
		model.addObject("stockCategory", category);

		Iterable<Ticket> tickets = cart.getCart();
		model.addObject("tickets", tickets);
		return model;
	}

	@RequestMapping(value = "/user/cleanShoppingCart", method = RequestMethod.GET)
	public ModelAndView cleanShoppingCart() {
		ModelAndView model = new ModelAndView("redirect:/user/shoppingCartList");
		cart.cleanup();
		return model;
	}

	@RequestMapping(value = "/user/confirmOrders", method = RequestMethod.GET)
	public ModelAndView confirmOrders(Principal principal) {
		ModelAndView model = new ModelAndView("/user/finish");
		for (int i = 0; i < cart.getSize(); i++) {
			Ticket ex = ticketDao.findOne(cart.isSold(i).getId());
			Customer x = customerDao.findOne(principal.getName());
			ex.setcId(x);
			ticketDao.save(ex);
		}
		cart.cleanup();
		return model;
	}

	@RequestMapping(value = "/user/shoppingList", method = RequestMethod.GET)
	public ModelAndView shoppingList(Principal principal) {
		ModelAndView model = new ModelAndView("/user/shoppinglist");
		Customer x = customerDao.findOne(principal.getName());
		model.addObject("tickets", x.getTickets());
		return model;
	}

	@RequestMapping(value = "/user/stockSearch", method = RequestMethod.POST)
	public ModelAndView stockSearch(@ModelAttribute("text") String text) {
		System.out.println("XXXX" + text);
		ModelAndView model = new ModelAndView("user/userlist");
		Iterable<StockCategory> categories = categoryDao.findAll();
		model.addObject("allStockCategories", categories);
		StockCategory category = categories.iterator().next();
		model.addObject("stockCategory", category);
		Iterable<StockHost> hosts = stockHostDao.findAll();
		model.addObject("allStockHosts", hosts);
		StockHost host = hosts.iterator().next();
		model.addObject("stockHost", host);

		Iterable<Stock> stocks = dao.findAll();
		ArrayList<Stock> found = new ArrayList<>();
		for (Stock stock : stocks) {
			if (stock.getName().equals(text)) {
				found.add(stock);
			}
		}
		model.addObject("stocks", found);
		return model;
	}

}
