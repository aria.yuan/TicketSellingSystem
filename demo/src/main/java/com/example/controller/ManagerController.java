package com.example.controller;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.example.entity.Schedule;
import com.example.entity.Stock;
import com.example.DAO.StockDAO;
import com.example.DAO.StockHostDAO;
import com.example.DAO.TicketDAO;
import com.example.DAO.ScheduleDAO;
import com.example.DAO.StockCategoryDAO;
import com.example.entity.StockCategory;
import com.example.entity.StockHost;
import com.example.entity.Ticket;
import com.example.storage.StorageService;

import antlr.collections.List;

@Controller
public class ManagerController {
	@Autowired
	// 利用@Autowired串連前面的CustomerDAO
	StockDAO dao;

	@Autowired
	StockCategoryDAO categoryDao;

	@Autowired
	TicketDAO ticketDao;

	@Autowired
	StockHostDAO stockHostDao;

	@Autowired
	ScheduleDAO scheduleDao;

	private final StorageService storageService;

	@Autowired
	public ManagerController(StorageService storageService) {
		this.storageService = storageService;
	}

	@RequestMapping(value = "/manager/stockCreate", method = RequestMethod.GET)
	// 无资料
	public ModelAndView openForm(@ModelAttribute Stock stock) {
		ModelAndView model = new ModelAndView("manager/stockCreate");
		Iterable<StockCategory> categories = categoryDao.findAll();
		model.addObject("allStockCategories", categories);
		Iterable<StockHost> hosts = stockHostDao.findAll();
		model.addObject("allStockHosts", hosts);
		return model;
	}

	@RequestMapping(value = "/manager/stockCreate", method = RequestMethod.POST)
	// 有资料
	public ModelAndView processForm(@Valid @ModelAttribute Stock sto,
			BindingResult bindingResult) {
		ModelAndView model = new ModelAndView(
				"redirect:/manager/stockRetrieveAll");// 指向下面的/stockRetrieveAll
		storageService.store(sto.getPhotoFile());
		sto.setPhoto();// copy file name to the field photo
		if (bindingResult.hasErrors()) {
			model = new ModelAndView("manager/stockCreate");
			Iterable<StockCategory> categories = categoryDao.findAll();
			model.addObject("allStockCategories", categories);
			Iterable<StockHost> hosts = stockHostDao.findAll();
			model.addObject("allStockHosts", hosts);
			return model;
		}

		dao.save(sto);
		model.addObject(sto);
		return model;
	}

	@RequestMapping(value = "/manager/scheduleCreate", method = RequestMethod.GET)
	// 无资料
	public ModelAndView openFormSchedule(
			@RequestParam(value = "id", required = false, defaultValue = "1") Long id) {
		ModelAndView model = new ModelAndView("manager/scheduleCreate");
		Iterable<Stock> stocks = dao.findAll();
		model.addObject("stocks", stocks);
		Stock stock = dao.findOne(id);
		model.addObject(stock);

		Schedule schedule = new Schedule();
		schedule.setsId(stock);
		model.addObject(schedule);
		return model;
	}

	@RequestMapping(value = "/manager/scheduleCreate", method = RequestMethod.POST)
	// 有资料
	public ModelAndView processFormSchedule(
			@Valid @ModelAttribute Schedule sche, BindingResult bindingResult) {

		// System.out.println("XXX" + sche.getsId().getName());
		scheduleDao.save(sche);

		for (int i = 0; i < sche.getCol() * sche.getRow(); i++) {
			Ticket ticket = new Ticket();
			ticket.setSeat_num(i + 1);
			ticket.setsId(sche);
			ticketDao.save(ticket);
		}
		ModelAndView model = new ModelAndView(
				"redirect:/manager/stockRetrieveAll");
		model.addObject(sche);
		return model;
	}

	@GetMapping("/manager/files/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

		Resource file = storageService.loadAsResource(filename);
		return ResponseEntity
				.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION,
						"attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}

	@RequestMapping(value = { "/manager/stockRetrieveAll" }, method = RequestMethod.GET)
	public ModelAndView retrieveCustomers() throws SQLException {
		ModelAndView model = new ModelAndView("manager/stockList");
		Iterable<StockCategory> categories = categoryDao.findAll();
		model.addObject("allStockCategories", categories);
		StockCategory category = categories.iterator().next();
		model.addObject("stockCategory", category);

		Iterable<StockHost> hosts = stockHostDao.findAll();
		model.addObject("allStockHosts", hosts);
		StockHost host = hosts.iterator().next();
		model.addObject("stockHost", host);

		Iterable<Stock> stocks = dao.findAll();
		model.addObject("stocks", stocks);
		return model;
	}

	@RequestMapping(value = { "/manager/stockRetrieveByCategory" }, method = RequestMethod.POST)
	public ModelAndView retrieveStocksByCategory(
			@RequestParam(value = "id", required = false, defaultValue = "1") Long id) {

		ModelAndView model = new ModelAndView("manager/stockList");

		Iterable<StockHost> hosts = stockHostDao.findAll();
		model.addObject("allStockHosts", hosts);
		StockHost host = hosts.iterator().next();
		model.addObject("stockHost", host);

		Iterable<StockCategory> categories = categoryDao.findAll();
		model.addObject("allStockCategories", categories);
		StockCategory category = categoryDao.findOne(id);
		model.addObject("stockCategory", category);

		model.addObject("stocks", category.getStocks());
		return model;
	}

	@RequestMapping(value = { "/manager/stockRetrieveByHost" }, method = RequestMethod.POST)
	public ModelAndView retrieveStocksByHost(
			@RequestParam(value = "id", required = false, defaultValue = "1") Long id) {

		ModelAndView model = new ModelAndView("manager/stockList");
		Iterable<StockCategory> categories = categoryDao.findAll();
		model.addObject("allStockCategories", categories);
		StockCategory category = categories.iterator().next();
		model.addObject("stockCategory", category);

		Iterable<StockHost> hosts = stockHostDao.findAll();
		model.addObject("allStockHosts", hosts);
		StockHost host = stockHostDao.findOne(id);
		model.addObject("stockHost", host);

		model.addObject("stocks", host.getStocks());
		return model;
	}

	@RequestMapping(value = "/manager/stockUpdate", method = RequestMethod.GET)
	public ModelAndView openFormUpdate(
			@RequestParam(value = "id", required = false, defaultValue = "1") Long id) {
		ModelAndView model = new ModelAndView("manager/stockUpdate");
		Stock stock = dao.findOne(id);
		model.addObject(stock);

		Iterable<StockCategory> categories = categoryDao.findAll();
		model.addObject("allStockCategories", categories);
		Iterable<StockHost> hosts = stockHostDao.findAll();
		model.addObject("allStockHosts", hosts);

		return model;
	}

	@RequestMapping(value = "/manager/stockUpdate", method = RequestMethod.POST)
	public ModelAndView processFormUpdate(@Valid @ModelAttribute Stock stock,
			BindingResult bindingResult) throws SQLException {
		ModelAndView model = new ModelAndView(
				"redirect:/manager/stockRetrieveAll");
		storageService.store(stock.getPhotoFile());
		stock.setPhoto();// copy file name to the field photo
		if (bindingResult.hasErrors()) {
			model = new ModelAndView("manager/stockUpdate");
			Iterable<StockCategory> categories = categoryDao.findAll();
			model.addObject("allStockCategories", categories);
			Iterable<StockHost> hosts = stockHostDao.findAll();
			model.addObject("allStockHosts", hosts);
			return model;
		}
		dao.save(stock);
		return model;
	}

	@RequestMapping(value = "/manager/scheduleUpdate", method = RequestMethod.GET)
	public ModelAndView openFormUpdatesche(
			@RequestParam(value = "id", required = false, defaultValue = "1") Long id) {
		ModelAndView model = new ModelAndView("manager/scheduleUpdate");
		Schedule schedule = scheduleDao.findOne(id);
		model.addObject(schedule);

		return model;
	}

	@RequestMapping(value = "/manager/scheduleUpdate", method = RequestMethod.POST)
	public ModelAndView processFormUpdatesche(
			@Valid @ModelAttribute Schedule sche, BindingResult bindingResult)
			throws SQLException {
		ModelAndView model = new ModelAndView("manager/scheduleDetail");
		if (bindingResult.hasErrors()) {
			model = new ModelAndView("manager/stockUpdate");
			Iterable<StockCategory> categories = categoryDao.findAll();
			model.addObject("allStockCategories", categories);
			Iterable<StockHost> hosts = stockHostDao.findAll();
			model.addObject("allStockHosts", hosts);
			return model;
		}
		scheduleDao.save(sche);
		if (sche.getCol() != 0 && sche.getRow() != 0) {
			Iterable<Ticket> tickets = sche.getTickets();
			for (Ticket ticket : tickets) {
				ticketDao.delete(ticket);
			}// 找出所有s_id为被修改场次的票，删除

			System.out.println("XXX" + sche.getCol());
			for (int i = 0; i < sche.getCol() * sche.getRow(); i++) {
				Ticket ticket = new Ticket();
				ticket.setSeat_num(i + 1);
				ticket.setsId(sche);
				ticketDao.save(ticket);
			}// 将新的票加入资料库

		}
		Stock stock = sche.getsId();
		model.addObject("stock", stock);
		Iterable<Schedule> schedules = stock.getSchedules();
		model.addObject("schedules", schedules);
		return model;
	}

	@RequestMapping(value = "/manager/stockDelete", method = RequestMethod.GET)
	public ModelAndView deleteCustomer(
			@RequestParam(value = "id", required = false, defaultValue = "1") Long id) {
		ModelAndView model = new ModelAndView(
				"redirect:/manager/stockRetrieveAll");
		Iterable<Schedule> schedules = dao.findOne(id).getSchedules();
		for (Schedule schedule : schedules) {
			scheduleDao.delete(schedule);
		}// 找出所有s_id为被修改场次的票，删除
		dao.delete(id);
		return model;
	}

	@RequestMapping(value = "/manager/scheduleDelete", method = RequestMethod.GET)
	public ModelAndView deleteSchedule(
			@RequestParam(value = "id", required = false, defaultValue = "1") Long id) {
		ModelAndView model = new ModelAndView(
				"redirect:/manager/stockRetrieveAll");
		Iterable<Ticket> tickets = scheduleDao.findOne(id).getTickets();
		for (Ticket ticket : tickets) {
			ticketDao.delete(ticket);
		}// 找出所有s_id为被修改场次的票，删除
		scheduleDao.delete(id);
		return model;
	}

	@RequestMapping(value = "/manager/scheduleDetail", method = RequestMethod.GET)
	public ModelAndView scheduleDetail(
			@RequestParam(value = "id", required = false) Long id) {
		ModelAndView model = new ModelAndView("manager/scheduleDetail");
		Stock stock = dao.findOne(id);
		model.addObject("stock", stock);
		Iterable<Schedule> schedules = stock.getSchedules();
		model.addObject("schedules", schedules);
		return model;
	}

	@RequestMapping(value = "/manager/stockDetail", method = RequestMethod.GET)
	public ModelAndView stockDetail(
			@RequestParam(value = "id", required = false) Long id) {
		ModelAndView model = new ModelAndView("manager/stockDetail");
		Schedule schedule = scheduleDao.findOne(id);
		model.addObject("schedule", schedule);
		ArrayList<Ticket> ticketSold = new ArrayList<>();
		ArrayList<Ticket> ticketUnsold = new ArrayList<>();
		for (int i = 0; i < schedule.getTicketSize(); i++) {
			if (schedule.getTicketByid(i).getIsSold() == 0) {
				System.out.println("lvhaoranshigongxuelaide");
				ticketUnsold.add(schedule.getTicketByid(i));
			} else {
				System.out.println("lvhaoranshigongxuelaide");
				ticketSold.add(schedule.getTicketByid(i));
			}
		}
		model.addObject("ticketSold", ticketSold);
		model.addObject("ticketUnsold", ticketUnsold);
		return model;
	}

}
