package com.example.controller;

import java.security.Principal;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.DAO.AuthoritiesDAO;
import com.example.DAO.CustomerDAO;
import com.example.entity.Authorities;
import com.example.entity.Customer;
import com.example.entity.Stock;
import com.example.entity.StockCategory;
import com.example.entity.Ticket;

/**
 * @author Joe Grandja
 */
@Controller
public class LoginController {
	@Autowired
	CustomerDAO customerDao;

	@Autowired
	AuthoritiesDAO authoritiesDao;

	@RequestMapping("/")
	public String root() {
		return "redirect:/index";
	}

	@RequestMapping("/index")
	public String index() {
		return "index";
	}

	@RequestMapping(value = "/user/index")
	public ModelAndView userIndex(Principal principal, HttpSession session) {
		ModelAndView model = new ModelAndView("redirect:/user/stockRetrieveAll");
		Customer x = customerDao.findOne(principal.getName());
		String name = x.getName();

		session.setAttribute("loginId", name);
		return model;
	}

	@RequestMapping("/manager/index")
	public String managerIndex() {
		return "redirect:/manager/stockRetrieveAll";
	}

	@RequestMapping("/login")
	public String login() {
		return "login";
	}

	@RequestMapping("/login-error")
	public String loginError(Model model) {
		model.addAttribute("loginError", true);
		return "login";
	}

	@RequestMapping(value = "/userRegister", method = RequestMethod.GET)
	// 无资料
	public ModelAndView openForm(@ModelAttribute Customer cus) {
		ModelAndView model = new ModelAndView("register");
		return model;
	}

	@RequestMapping(value = "/userRegister", method = RequestMethod.POST)
	// 有资料
	public ModelAndView processForm(@Valid @ModelAttribute Customer cus,
			BindingResult bindingResult) {
		ModelAndView model = new ModelAndView("redirect:/login");
		if (bindingResult.hasErrors()) {
			model = new ModelAndView("/userRegister");
			return model;
		}
		cus.setEnabled(1);
		customerDao.save(cus);
		Authorities aut = new Authorities();
		aut.setUsername(cus.getUsername());
		aut.setAuthority("ROLE_USER");
		authoritiesDao.save(aut);

		model.addObject(cus);
		return model;
	}

}